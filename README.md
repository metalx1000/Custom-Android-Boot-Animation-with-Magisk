
# Custom-Android-Boot-Animation-with-Magisk

Copyright Kris Occhipinti 2022-10-15

(https://filmsbykris.com)

License GPLv3

In the below example I use "/system/product/media" as the directory for the animation.
This is how it is for my Motorola Phone, but many other phones use "/system/media"

```bash
#download example animation
wget "https://gitlab.com/metalx1000/Custom-Android-Boot-Animation-with-Magisk/-/raw/master/animations/bootanimation.zip"

#create Magisk Module Folder
adb shell "su -c 'mkdir -p /data/adb/modules/custombootani/system/product/media'"

#copy animation to device
adb push bootanimation.zip /sdcard/
adb shell "su -c 'mv /sdcard/bootanimation.zip /data/adb/modules/custombootani/system/product/media/'"

#reboot
adb reboot
```

You may wabt to load the Magisk App and click on modules to make sure it sees it before rebooting.


# Turn a video into a Boot Animation
```bash
#!/bin/bash
###################################################################### 
#Copyright (C) 2022  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

video="https://www.youtube.com/watch?v=Nl6l63AYg7s"
fps=10
background="#000000"

adb shell wm size || exit 1
rez="$(adb shell wm size|awk '{print $3}')"
#rez="313x720"
x="$(echo $rez|cut -d\x -f1)"
y="$(echo $rez|cut -d\x -f2)"

echo "Creating Work Director..."
mkdir bootani || exit 1
cd bootani

echo "$x $y $fps" > desc.txt
echo "p 0 0 part0 $background -1" >> desc.txt

mkdir part0
cd part0
youtube-dl "$video" -o "video.%(ext)s"
ffmpeg -i video.* -vf fps=10 Bootup_%05d.jpg
rm video.*


#mogrify -resize $x -background black -gravity center -extent $rez -rotate "90" *.jpg
mogrify -rotate "90" *.jpg

cd ../
zip bootanimation.zip * -r -0

adb push bootanimation.zip /sdcard/
adb shell "su -c 'mv /sdcard/bootanimation.zip /data/adb/modules/custombootani/system/product/media/'"

#reboot
adb reboot
```
